package com.hristiyantodorov.androidmoviedb.binding;

import android.view.View;

public class BindingUtils {

    public static int isVisible(boolean isVisible) {
        return isVisible ? View.VISIBLE : View.GONE;
    }
}
