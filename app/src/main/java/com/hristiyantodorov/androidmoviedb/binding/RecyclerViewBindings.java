package com.hristiyantodorov.androidmoviedb.binding;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.BindingAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.hristiyantodorov.androidmoviedb.App;
import com.hristiyantodorov.androidmoviedb.adapter.BaseDiffUtilAdapter;
import com.hristiyantodorov.androidmoviedb.adapter.diffutil.DiffComparable;
import com.hristiyantodorov.androidmoviedb.adapter.diffutil.ItemDiffUtil;

import java.util.List;

public class RecyclerViewBindings {

    @BindingAdapter({"items", "layoutId"})
    public static <T extends DiffComparable<T>> void setDiffUtilBindings(RecyclerView view, MutableLiveData<List<T>> items, int itemLayoutId) {
        if (view.getLayoutManager() == null) {
            view.setLayoutManager(
                    new LinearLayoutManager(view.getContext(), RecyclerView.HORIZONTAL, false)
            );
        }

        RecyclerView.Adapter previousAdapter = view.getAdapter();

        if (previousAdapter == null) {
            BaseDiffUtilAdapter<T> adapter = new BaseDiffUtilAdapter<>(new ItemDiffUtil<T>(), itemLayoutId);
            view.setAdapter(adapter);
        } else if (items.getValue() != null) {
            ((BaseDiffUtilAdapter) view.getAdapter()).submitList(items.getValue());
        }

        if (previousAdapter != null) {
            App.getRefWatcher(view.getContext()).watch(previousAdapter);
        }
    }
}
