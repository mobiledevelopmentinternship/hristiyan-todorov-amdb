package com.hristiyantodorov.androidmoviedb.binding;

import android.arch.lifecycle.MutableLiveData;
import android.databinding.BindingAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;

import com.hristiyantodorov.androidmoviedb.App;
import com.hristiyantodorov.androidmoviedb.adapter.BasePagerAdapter;

import java.util.List;

public class ViewPagerBindings {

    @BindingAdapter({"items", "layoutId"})
    public static <T> void setPagerAdapter(ViewPager viewPager, MutableLiveData<List<T>> items, int layoutId) {
        PagerAdapter previousAdapter = viewPager.getAdapter();

        if (previousAdapter == null && items.getValue() != null) {
            BasePagerAdapter<T> pagerAdapter = new BasePagerAdapter<>(items.getValue(), layoutId);
            viewPager.setAdapter(pagerAdapter);
        }

        if (previousAdapter != null) {
            App.getRefWatcher(viewPager.getContext()).watch(previousAdapter);
        }
    }

    @BindingAdapter("onPageChangedListener")
    public static void setOnPageChangeListener(ViewPager viewPager, ViewPager.OnPageChangeListener listener) {
        viewPager.clearOnPageChangeListeners();
        viewPager.addOnPageChangeListener(listener);
    }
}
