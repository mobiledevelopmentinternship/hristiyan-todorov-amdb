package com.hristiyantodorov.androidmoviedb.binding;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.hristiyantodorov.androidmoviedb.BuildConfig;
import com.hristiyantodorov.androidmoviedb.R;

public class ImageViewBindings {

    @BindingAdapter("path")
    public static void loadImage(ImageView view, String url) {
        Glide.with(view.getContext())
                .load(BuildConfig.PosterWidth500UrlPrefix + url)
                .placeholder(R.drawable.ic_placeholder)
                .skipMemoryCache(false)
                .into(view);
    }
}