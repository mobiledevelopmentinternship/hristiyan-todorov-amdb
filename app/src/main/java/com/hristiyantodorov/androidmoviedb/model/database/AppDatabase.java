package com.hristiyantodorov.androidmoviedb.model.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.hristiyantodorov.androidmoviedb.model.database.movie.MovieDao;
import com.hristiyantodorov.androidmoviedb.model.database.movie.MovieDbModel;
import com.hristiyantodorov.androidmoviedb.model.database.person.PersonDao;
import com.hristiyantodorov.androidmoviedb.model.database.person.PersonDbModel;
import com.hristiyantodorov.androidmoviedb.model.database.series.SeriesDao;
import com.hristiyantodorov.androidmoviedb.model.database.series.SeriesDbModel;

@Database(entities = {
        MovieDbModel.class,
        SeriesDbModel.class,
        PersonDbModel.class},
        version = 1)
public abstract class AppDatabase extends RoomDatabase {

    public abstract MovieDao movieDao();

    public abstract SeriesDao seriesDao();

    public abstract PersonDao personDao();
}
