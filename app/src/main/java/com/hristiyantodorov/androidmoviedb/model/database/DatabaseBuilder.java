package com.hristiyantodorov.androidmoviedb.model.database;

import android.app.Application;
import android.arch.persistence.room.Room;

import com.hristiyantodorov.androidmoviedb.model.database.movie.MovieDao;
import com.hristiyantodorov.androidmoviedb.model.database.person.PersonDao;
import com.hristiyantodorov.androidmoviedb.model.database.series.SeriesDao;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DatabaseBuilder {

    private AppDatabase database;

    @Inject
    public DatabaseBuilder(Application application) {
        this.database = Room.databaseBuilder(application, AppDatabase.class, "movies_db")
                .build();
    }

    public AppDatabase getDatabase() {
        return database;
    }

    public MovieDao getMovieDao() {
        return database.movieDao();
    }

    public SeriesDao getSeriesDao() {
        return database.seriesDao();
    }

    public PersonDao getPersonDao() {
        return database.personDao();
    }
}