package com.hristiyantodorov.androidmoviedb.model.database.person;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface PersonDao {

    @Query("SELECT * FROM people")
    List<PersonDbModel> getPeople();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertPerson(PersonDbModel personDbModel);

    @Query("DELETE FROM people")
    void deleteAllPeople();
}
