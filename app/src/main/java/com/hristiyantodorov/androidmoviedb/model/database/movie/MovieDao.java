package com.hristiyantodorov.androidmoviedb.model.database.movie;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface MovieDao {

    @Query("SELECT * FROM movies")
    List<MovieDbModel> getMovies();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertMovie(MovieDbModel movie);

    @Query("DELETE FROM movies")
    void deleteAllMovies();
}