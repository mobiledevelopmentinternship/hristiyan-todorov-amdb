package com.hristiyantodorov.androidmoviedb.model.database.series;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "series")
public class SeriesDbModel {

    @PrimaryKey()
    @ColumnInfo(name = "s_id")
    private Integer id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "original_name")
    private String originalName;
    @ColumnInfo(name = "number_of_episodes")
    private Integer numberOfEpisodes;
    @ColumnInfo(name = "number_of_seasons")
    private Integer numberOfSeasons;
    @ColumnInfo(name = "overview")
    private String overview;
    @ColumnInfo(name = "poster_url")
    private String posterPath;
    @ColumnInfo(name = "type")
    private String type;
    @ColumnInfo(name = "vote_average")
    private Double voteAverage;
    @ColumnInfo(name = "vote_count")
    private Integer voteCount;

    @Ignore
    public SeriesDbModel() {
    }

    public SeriesDbModel(Integer id, String name, String originalName, Integer numberOfEpisodes,
                         Integer numberOfSeasons, String overview, String posterPath,
                         String type, Double voteAverage, Integer voteCount) {
        this.id = id;
        this.name = name;
        this.originalName = originalName;
        this.numberOfEpisodes = numberOfEpisodes;
        this.numberOfSeasons = numberOfSeasons;
        this.overview = overview;
        this.posterPath = posterPath;
        this.type = type;
        this.voteAverage = voteAverage;
        this.voteCount = voteCount;
    }

    private SeriesDbModel(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setOriginalName(builder.originalName);
        setNumberOfEpisodes(builder.numberOfEpisodes);
        setNumberOfSeasons(builder.numberOfSeasons);
        setOverview(builder.overview);
        setPosterPath(builder.posterPath);
        setType(builder.type);
        setVoteAverage(builder.voteAverage);
        setVoteCount(builder.voteCount);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public Integer getNumberOfEpisodes() {
        return numberOfEpisodes;
    }

    public void setNumberOfEpisodes(Integer numberOfEpisodes) {
        this.numberOfEpisodes = numberOfEpisodes;
    }

    public Integer getNumberOfSeasons() {
        return numberOfSeasons;
    }

    public void setNumberOfSeasons(Integer numberOfSeasons) {
        this.numberOfSeasons = numberOfSeasons;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Double voteAverage) {
        this.voteAverage = voteAverage;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public static final class Builder {
        private Integer id;
        private String name;
        private String originalName;
        private Integer numberOfEpisodes;
        private Integer numberOfSeasons;
        private String overview;
        private String posterPath;
        private String type;
        private Double voteAverage;
        private Integer voteCount;

        public Builder() {
        }

        public Builder id(Integer val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder originalName(String val) {
            originalName = val;
            return this;
        }

        public Builder numberOfEpisodes(Integer val) {
            numberOfEpisodes = val;
            return this;
        }

        public Builder numberOfSeasons(Integer val) {
            numberOfSeasons = val;
            return this;
        }

        public Builder overview(String val) {
            overview = val;
            return this;
        }

        public Builder posterPath(String val) {
            posterPath = val;
            return this;
        }

        public Builder type(String val) {
            type = val;
            return this;
        }

        public Builder voteAverage(Double val) {
            voteAverage = val;
            return this;
        }

        public Builder voteCount(Integer val) {
            voteCount = val;
            return this;
        }

        public SeriesDbModel build() {
            return new SeriesDbModel(this);
        }
    }
}