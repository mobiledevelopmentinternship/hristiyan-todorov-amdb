package com.hristiyantodorov.androidmoviedb.model.database.series;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface SeriesDao {

    @Query("SELECT * FROM series")
    List<SeriesDbModel> getSeries();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertSeries(SeriesDbModel seriesDbModel);

    @Query("DELETE FROM series")
    void deleteAllSeries();
}
