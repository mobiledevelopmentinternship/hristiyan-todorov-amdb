package com.hristiyantodorov.androidmoviedb.model.database.person;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "people")
public class PersonDbModel {

    @PrimaryKey()
    @ColumnInfo(name = "p_id")
    private Integer id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "known_for_dep")
    private String knownForDepartment;
    @ColumnInfo(name = "img_url")
    private String profilePath;
    @ColumnInfo(name = "gender")
    private Integer gender;
    @ColumnInfo(name = "biography")
    private String biography;
    @ColumnInfo(name = "place_of_birth")
    private String placeOfBirth;
    @ColumnInfo(name = "birthday")
    private String birthday;
    @ColumnInfo(name = "deathday")
    private String deathday;

    @Ignore
    public PersonDbModel() {
    }

    private PersonDbModel(Builder builder) {
        setId(builder.id);
        setName(builder.name);
        setKnownForDepartment(builder.knownForDepartment);
        setProfilePath(builder.profilePath);
        setGender(builder.gender);
        setBiography(builder.biography);
        setPlaceOfBirth(builder.placeOfBirth);
        setBirthday(builder.birthday);
        setDeathday(builder.deathday);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKnownForDepartment() {
        return knownForDepartment;
    }

    public void setKnownForDepartment(String knownForDepartment) {
        this.knownForDepartment = knownForDepartment;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public void setProfilePath(String profilePath) {
        this.profilePath = profilePath;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getDeathday() {
        return deathday;
    }

    public void setDeathday(String deathday) {
        this.deathday = deathday;
    }

    public static final class Builder {

        private Integer id;
        private String name;
        private String knownForDepartment;
        private String profilePath;
        private Integer gender;
        private String biography;
        private String placeOfBirth;
        private String birthday;
        private String deathday;

        public Builder() {
        }

        public Builder id(Integer val) {
            id = val;
            return this;
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder knownForDepartment(String val) {
            knownForDepartment = val;
            return this;
        }

        public Builder profilePath(String val) {
            profilePath = val;
            return this;
        }

        public Builder gender(Integer val) {
            gender = val;
            return this;
        }

        public Builder biography(String val) {
            biography = val;
            return this;
        }

        public Builder placeOfBirth(String val) {
            placeOfBirth = val;
            return this;
        }

        public Builder birthday(String val) {
            birthday = val;
            return this;
        }

        public Builder deathday(String val) {
            deathday = val;
            return this;
        }

        public PersonDbModel build() {
            return new PersonDbModel(this);
        }
    }
}