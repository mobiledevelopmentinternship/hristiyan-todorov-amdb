package com.hristiyantodorov.androidmoviedb.model.database.movie;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "movies")
public class MovieDbModel {

    @PrimaryKey()
    @ColumnInfo(name = "m_id")
    private Integer id;
    @ColumnInfo(name = "title")
    private String title;
    @ColumnInfo(name = "poster_url")
    private String posterPath;
    @ColumnInfo(name = "vote_count")
    private Integer voteCount;
    @ColumnInfo(name = "vote_average")
    private Float voteAverage;
    @ColumnInfo(name = "original_title")
    private String originalTitle;
    @ColumnInfo(name = "backdrop_path")
    private String backdropPath;
    @ColumnInfo(name = "overview")
    private String overview;
    @ColumnInfo(name = "release_date")
    private String releaseDate;

    @Ignore
    public MovieDbModel() {
    }

    public MovieDbModel(Integer id, String title, String posterPath, Integer voteCount,
                        Float voteAverage, String originalTitle, String backdropPath,
                        String overview, String releaseDate) {
        this.id = id;
        this.title = title;
        this.posterPath = posterPath;
        this.voteCount = voteCount;
        this.voteAverage = voteAverage;
        this.originalTitle = originalTitle;
        this.backdropPath = backdropPath;
        this.overview = overview;
        this.releaseDate = releaseDate;
    }

    private MovieDbModel(Builder builder) {
        setId(builder.id);
        setTitle(builder.title);
        setPosterPath(builder.posterPath);
        setVoteCount(builder.voteCount);
        setVoteAverage(builder.voteAverage);
        setOriginalTitle(builder.originalTitle);
        setBackdropPath(builder.backdropPath);
        setOverview(builder.overview);
        setReleaseDate(builder.releaseDate);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public Integer getVoteCount() {
        return voteCount;
    }

    public void setVoteCount(Integer voteCount) {
        this.voteCount = voteCount;
    }

    public Float getVoteAverage() {
        return voteAverage;
    }

    public void setVoteAverage(Float voteAverage) {
        this.voteAverage = voteAverage;
    }

    public String getOriginalTitle() {
        return originalTitle;
    }

    public void setOriginalTitle(String originalTitle) {
        this.originalTitle = originalTitle;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public static final class Builder {
        private Integer id;
        private String title;
        private String posterPath;
        private Integer voteCount;
        private Float voteAverage;
        private String originalTitle;
        private String backdropPath;
        private String overview;
        private String releaseDate;

        public Builder() {
        }

        public Builder id(Integer val) {
            id = val;
            return this;
        }

        public Builder title(String val) {
            title = val;
            return this;
        }

        public Builder posterPath(String val) {
            posterPath = val;
            return this;
        }

        public Builder voteCount(Integer val) {
            voteCount = val;
            return this;
        }

        public Builder voteAverage(Float val) {
            voteAverage = val;
            return this;
        }

        public Builder originalTitle(String val) {
            originalTitle = val;
            return this;
        }

        public Builder backdropPath(String val) {
            backdropPath = val;
            return this;
        }

        public Builder overview(String val) {
            overview = val;
            return this;
        }

        public Builder releaseDate(String val) {
            releaseDate = val;
            return this;
        }

        public MovieDbModel build() {
            return new MovieDbModel(this);
        }
    }
}
