package com.hristiyantodorov.androidmoviedb.di.module.activity;

import com.hristiyantodorov.androidmoviedb.ui.activity.main.MainActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector
    abstract MainActivity bindMainActivity();
}
