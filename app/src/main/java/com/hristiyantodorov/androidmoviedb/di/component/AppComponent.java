package com.hristiyantodorov.androidmoviedb.di.component;


import com.hristiyantodorov.androidmoviedb.App;
import com.hristiyantodorov.androidmoviedb.di.module.activity.ActivityModule;
import com.hristiyantodorov.androidmoviedb.di.module.application.ApplicationModule;
import com.hristiyantodorov.androidmoviedb.di.module.fragment.FragmentModule;
import com.hristiyantodorov.androidmoviedb.di.module.viewmodel.ViewModelModule;

import javax.inject.Singleton;

import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;

@Singleton
@Component(
        modules = {
                ApplicationModule.class,
                ActivityModule.class,
                FragmentModule.class,
                AndroidInjectionModule.class,
                ViewModelModule.class
        }
)
public interface AppComponent extends AndroidInjector<App> {

    @Component.Factory
    abstract class Factory implements AndroidInjector.Factory<App> {
    }
}