package com.hristiyantodorov.androidmoviedb.di.module.application;

import android.content.Context;

import com.hristiyantodorov.androidmoviedb.App;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    @Singleton
    @Provides
    Context provideContext(App application) {
        return application.getApplicationContext();
    }
}
