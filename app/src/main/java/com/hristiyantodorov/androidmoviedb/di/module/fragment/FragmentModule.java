package com.hristiyantodorov.androidmoviedb.di.module.fragment;


import com.hristiyantodorov.androidmoviedb.ui.fragment.actordetails.ActorDetailsFragment;
import com.hristiyantodorov.androidmoviedb.ui.fragment.filmographylist.FilmographyListFragment;
import com.hristiyantodorov.androidmoviedb.ui.fragment.main.MainFragment;
import com.hristiyantodorov.androidmoviedb.ui.fragment.moviedetails.MovieDetailsFragment;
import com.hristiyantodorov.androidmoviedb.ui.fragment.reviewslist.ReviewsListFragment;
import com.hristiyantodorov.androidmoviedb.ui.fragment.search.SearchFragment;
import com.hristiyantodorov.androidmoviedb.ui.fragment.seriesdetails.SeriesDetailsFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentModule {

    @ContributesAndroidInjector
    abstract ActorDetailsFragment provideActorDetailsFragment();

    @ContributesAndroidInjector
    abstract FilmographyListFragment provideFilmographyListFragment();

    @ContributesAndroidInjector
    abstract MainFragment provideMainFragment();

    @ContributesAndroidInjector
    abstract MovieDetailsFragment provideMovieDetailsFragment();

    @ContributesAndroidInjector
    abstract ReviewsListFragment provideReviewsListFragment();

    @ContributesAndroidInjector
    abstract SearchFragment provideSearchFragment();

    @ContributesAndroidInjector
    abstract SeriesDetailsFragment provideSeriesDetailsFragment();
}
