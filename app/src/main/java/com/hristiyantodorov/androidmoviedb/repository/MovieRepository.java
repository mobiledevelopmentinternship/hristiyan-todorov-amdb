package com.hristiyantodorov.androidmoviedb.repository;

import com.hristiyantodorov.androidmoviedb.BuildConfig;
import com.hristiyantodorov.androidmoviedb.model.apiresponse.PopularList;
import com.hristiyantodorov.androidmoviedb.model.apiresponse.TrendingList;
import com.hristiyantodorov.androidmoviedb.retrofit.APIClient;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Single;

@Singleton
public class MovieRepository {

    private APIClient apiClient;

    @Inject
    MovieRepository(APIClient apiClient) {
        this.apiClient = apiClient;
    }

    public Single<TrendingList> getTrendingList() {
        return apiClient.getService().getTrendingList(BuildConfig.ApiVersion, BuildConfig.ApiKey);
    }

    public Single<PopularList> getPopularList() {
        return apiClient.getService().getPopularList(BuildConfig.ApiVersion, BuildConfig.ApiKey);
    }
}
