package com.hristiyantodorov.androidmoviedb.service;

import com.hristiyantodorov.androidmoviedb.model.apiresponse.PopularList;
import com.hristiyantodorov.androidmoviedb.model.apiresponse.TrendingList;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("{api_version}/movie/popular")
    Single<PopularList> getPopularList(@Path("api_version") String apiVersion,
                                       @Query("api_key") String apiKey);

    @GET("{api_version}/trending/all/week")
    Single<TrendingList> getTrendingList(@Path("api_version") String apiVersion,
                                         @Query("api_key") String apiKey);
}
