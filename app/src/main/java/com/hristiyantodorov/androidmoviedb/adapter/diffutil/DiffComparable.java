package com.hristiyantodorov.androidmoviedb.adapter.diffutil;

/**
 * Implemented by the model classes in order to provide DiffUtil with a way of
 * comparing objects and their contents.
 *
 * @param <T> model's type
 */
public interface DiffComparable<T> {

    boolean isItemTheSameAs(T newItem);

    boolean areContentsTheSameAs(T newItem);
}
