package com.hristiyantodorov.androidmoviedb.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hristiyantodorov.androidmoviedb.BR;

import java.util.List;

public class BasePagerAdapter<T> extends PagerAdapter {

    private List<T> items;
    private int layoutId;

    public BasePagerAdapter(List<T> items, int layoutId) {
        this.items = items;
        this.layoutId = layoutId;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        T item = items.get(position);
        ViewDataBinding bindingUtil = DataBindingUtil.inflate(
                LayoutInflater.from(container.getContext()),
                layoutId,
                container,
                false);
        bindingUtil.setVariable(BR.item, item);
        bindingUtil.executePendingBindings();

        container.addView(bindingUtil.getRoot());
        return bindingUtil;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object view) {
        ViewDataBinding binding = (ViewDataBinding) view;
        binding.executePendingBindings();
        container.removeView(binding.getRoot());
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return ((ViewDataBinding) object).getRoot() == view;
    }
}