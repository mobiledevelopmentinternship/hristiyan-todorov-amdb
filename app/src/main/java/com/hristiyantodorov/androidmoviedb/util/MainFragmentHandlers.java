package com.hristiyantodorov.androidmoviedb.util;

import android.arch.lifecycle.MutableLiveData;
import android.support.v4.view.ViewPager;

public class MainFragmentHandlers implements ViewPager.OnPageChangeListener {

    private ViewPager viewPager;

    public MutableLiveData<Boolean> isButtonLeftVisible = new MutableLiveData<>();
    public MutableLiveData<Boolean> isButtonRightVisible = new MutableLiveData<>();

    public MainFragmentHandlers(ViewPager viewPager) {
        this.viewPager = viewPager;
        isButtonRightVisible.setValue(true);
    }

    public void onLeftArrowClicked() {
        viewPager.setCurrentItem(getItem(viewPager, -1), true);
    }

    public void onRightArrowClicked() {
        viewPager.setCurrentItem(getItem(viewPager, +1), true);
    }

    private int getItem(ViewPager viewPager, int i) {
        return viewPager.getCurrentItem() + i;
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {
        // Not used.
    }

    @Override
    public void onPageSelected(int position) {
        if (position == viewPager.getAdapter().getCount() - 1) {
            isButtonRightVisible.postValue(false);
            isButtonLeftVisible.postValue(true);
        } else if (position == 0) {
            isButtonRightVisible.postValue(true);
            isButtonLeftVisible.postValue(false);
        } else {
            isButtonRightVisible.postValue(true);
            isButtonLeftVisible.postValue(true);
        }
    }

    @Override
    public void onPageScrollStateChanged(int i) {
        // Not used.
    }
}
