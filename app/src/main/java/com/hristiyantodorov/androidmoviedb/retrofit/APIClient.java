package com.hristiyantodorov.androidmoviedb.retrofit;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.hristiyantodorov.androidmoviedb.BuildConfig;
import com.hristiyantodorov.androidmoviedb.service.ApiService;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Singleton
public class APIClient {

    private ApiService service;
    private static final int REQUEST_TIMEOUT = 60;
    private static final Object LOCK = new Object();

    @Inject
    public APIClient() {
    }

    private Gson createGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        return gsonBuilder.create();
    }

    private OkHttpClient buildOkHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder()
                .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(interceptor);
        return httpClient.build();
    }

    private Retrofit buildRetrofit(Gson gson, OkHttpClient okHttpClient) {
        Retrofit.Builder retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BaseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient);

        return retrofit.build();
    }

    public ApiService getService() {
        if (service == null) {
            synchronized (LOCK) {
                if (service == null) {
                    Retrofit retrofit = buildRetrofit(createGson(), buildOkHttpClient());
                    service = retrofit.create(ApiService.class);
                }
            }
        }
        return service;
    }
}