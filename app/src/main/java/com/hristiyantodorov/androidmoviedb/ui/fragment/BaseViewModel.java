package com.hristiyantodorov.androidmoviedb.ui.fragment;

import android.arch.lifecycle.ViewModel;

import com.hristiyantodorov.androidmoviedb.util.DisposableManager;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;

public abstract class BaseViewModel extends ViewModel {

    private DisposableManager disposableManager;

    protected BaseViewModel() {
        this.disposableManager = new DisposableManager();
    }

    protected <T> Disposable subscribeSingle(Single<T> singleObservable, Consumer<T> onSuccess, Consumer<Throwable> onError) {
        return disposableManager.subscribeSingle(singleObservable, onSuccess, onError);
    }

    protected void subscribeCompletable(Completable completable) {
        disposableManager.subscribeCompletable(completable);
    }

    protected void subscribeCompletable(Completable completable, Action onComplete) {
        disposableManager.subscribeCompletable(completable, onComplete);
    }

    protected void addToCompositeDisposable(Disposable disposable) {
        disposableManager.add(disposable);
    }

    @Override
    protected void onCleared() {
        disposableManager.dispose();
    }
}
