package com.hristiyantodorov.androidmoviedb.ui.activity.main;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.hristiyantodorov.androidmoviedb.R;
import com.hristiyantodorov.androidmoviedb.databinding.ActivityMainBinding;
import com.hristiyantodorov.androidmoviedb.ui.activity.BaseActivity;

import java.util.HashSet;
import java.util.Set;

import static androidx.navigation.ui.NavigationUI.setupWithNavController;

public class MainActivity extends BaseActivity<ActivityMainBinding>
        implements AppBarConfiguration.OnNavigateUpListener {

    private DrawerLayout drawerLayout;
    private NavController navController;
    private AppBarConfiguration appBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        drawerLayout = getDataBinding().drawerLayout;

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        setupNavigationMenu(navController);

        appBarConfiguration = new AppBarConfiguration.Builder(getConfigItems())
                .setDrawerLayout(drawerLayout)
                .build();
        setupActionBar(navController, appBarConfiguration);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_main;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(navController, appBarConfiguration);
    }

    private void setupNavigationMenu(NavController navController) {
        NavigationView navigationView = getDataBinding().navView;
        setupWithNavController(navigationView, navController);
    }

    private void setupActionBar(NavController navController, AppBarConfiguration appBarConfiguration) {
        setupWithNavController(getDataBinding().toolbar, navController, appBarConfiguration);
    }

    /**
     * Specifies the set of top level destinations. The Up button will
     * not be displayed when on these destinations.
     *
     * @return Set<Integer>
     */
    private Set<Integer> getConfigItems() {
        Set<Integer> appBarConfigItems = new HashSet<>();
        appBarConfigItems.add(R.id.nav_home);
        appBarConfigItems.add(R.id.nav_movie_details);
        appBarConfigItems.add(R.id.nav_actor_details);
        appBarConfigItems.add(R.id.nav_series_details);
        appBarConfigItems.add(R.id.nav_search);
        appBarConfigItems.add(R.id.nav_filmography);
        appBarConfigItems.add(R.id.nav_reviews_list);
        return appBarConfigItems;
    }
}
