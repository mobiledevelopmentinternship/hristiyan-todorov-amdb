package com.hristiyantodorov.androidmoviedb.ui.fragment.main;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hristiyantodorov.androidmoviedb.R;
import com.hristiyantodorov.androidmoviedb.databinding.FragmentMainBinding;
import com.hristiyantodorov.androidmoviedb.ui.fragment.BaseFragment;
import com.hristiyantodorov.androidmoviedb.util.MainFragmentHandlers;

public class MainFragment extends BaseFragment<FragmentMainBinding, MainViewModel> {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        getDataBinding().setHandlers(new MainFragmentHandlers(getDataBinding().viewPagerTrending));

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.loadMainData();
    }

    @Override
    protected Class<MainViewModel> getViewModel() {
        return MainViewModel.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main;
    }
}