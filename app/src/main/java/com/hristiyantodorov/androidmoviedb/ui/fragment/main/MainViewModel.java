package com.hristiyantodorov.androidmoviedb.ui.fragment.main;

import android.arch.lifecycle.MutableLiveData;

import com.hristiyantodorov.androidmoviedb.model.apiresponse.MovieResponse;
import com.hristiyantodorov.androidmoviedb.repository.MovieRepository;
import com.hristiyantodorov.androidmoviedb.ui.fragment.BaseViewModel;

import java.util.List;

import javax.inject.Inject;

import timber.log.Timber;

public class MainViewModel extends BaseViewModel {

    private MutableLiveData<List<MovieResponse>> moviesPopular = new MutableLiveData<>();
    private MutableLiveData<List<MovieResponse>> moviesTrending = new MutableLiveData<>();

    @Inject
    MovieRepository repository;

    @Inject
    MainViewModel() {
    }

    public MutableLiveData<List<MovieResponse>> getMoviesPopular() {
        return moviesPopular;
    }

    public MutableLiveData<List<MovieResponse>> getMoviesTrending() {
        return moviesTrending;
    }

    public void loadMainData() {
        loadPopularList();
        loadTrendingList();
    }

    private void loadPopularList() {
        addToCompositeDisposable(
                subscribeSingle(repository.getPopularList(),
                        popularList -> moviesPopular.postValue(popularList.getResults()),
                        Timber::d)
        );
    }

    private void loadTrendingList() {
        addToCompositeDisposable(
                subscribeSingle(repository.getTrendingList(),
                        trendingList -> moviesTrending.postValue(trendingList.getResults()),
                        Timber::d)
        );
    }
}