package com.hristiyantodorov.androidmoviedb.ui.activity;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;

import androidx.navigation.ui.AppBarConfiguration;

import dagger.android.AndroidInjection;
import dagger.android.support.DaggerAppCompatActivity;

public abstract class BaseActivity<B extends ViewDataBinding> extends DaggerAppCompatActivity
        implements AppBarConfiguration.OnNavigateUpListener {

    private B viewDataBinding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AndroidInjection.inject(this);
        viewDataBinding = DataBindingUtil.setContentView(this, getLayoutResId());
    }

    @LayoutRes
    protected abstract int getLayoutResId();

    public B getDataBinding() {
        return viewDataBinding;
    }
}
