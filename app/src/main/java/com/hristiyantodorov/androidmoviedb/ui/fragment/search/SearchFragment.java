package com.hristiyantodorov.androidmoviedb.ui.fragment.search;

import com.hristiyantodorov.androidmoviedb.R;
import com.hristiyantodorov.androidmoviedb.databinding.FragmentSearchBinding;
import com.hristiyantodorov.androidmoviedb.ui.fragment.BaseFragment;

public class SearchFragment extends BaseFragment<FragmentSearchBinding, SearchViewModel> {

    @Override
    protected Class<SearchViewModel> getViewModel() {
        return SearchViewModel.class;
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_search;
    }
}
